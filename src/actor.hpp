#pragma once

#include <memory>

#include <SFML/Graphics.hpp>

namespace space_invaders {

class Actor {
 public:
  Actor() = default;
};

}  // namespace space_invaders